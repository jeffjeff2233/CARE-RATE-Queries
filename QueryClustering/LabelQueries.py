"""
Script Description:
Conducts the following operations:
1.Reads from a list of queries and googles them.
2.Classifies each of the search results.
3.Writes these predictions to a json file for query clustering later.
"""

import urllib
from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import TfidfVectorizer
import re
import pickle
import LinkExtractor
import GroupNeuralModel
import json


# Variable declaration and set up.
dump = "D:\\QueryLabels\\"

labels_dir = "C:\\Users\\Lanius\\work\\CARE-RATE-Query-Classifier\\QueryExtractor\\queryDump\\"

dp = open('D:\\QueryLabels\\labeldump900-999.json', 'w+', 2 ** 31 - 1)
dp.write("{\"labels\":[")

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6)'
                  ' AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}

with open('new_negative_dat.pkl', 'rb') as f:
    rdat = pickle.load(f)

with open('svcl.pkl', 'rb') as f:
    model = pickle.load(f)

print("Creating tfidf features")
terms = TfidfVectorizer(ngram_range=(1, 2), max_features=10000)
terms.fit(rdat["X"])

# Save the Vectorizor for later use.
with open('TFIDFFeatures.pkl', 'wb') as f:
    pickle.dump(terms, f, -1)

big_model = GroupNeuralModel.GroupNeuralModel("./neuralnet_newNegative_topic0.h5", "./neuralnet_newNegative_topic1.h5",
                                              "./neuralnet_newNegative_topic2.h5", "./neuralnet_newNegative_topic3.h5")

pattern = re.compile(r"(?is)<script[^>]*>(.*?)</script>")


queries = open(labels_dir + "relevant_anchor_0.txt").readlines()

extractor = LinkExtractor.LinkExtractor()

ARTICLES_BEGIN = 900

ARTICLES_END = 999

# Loop over a selected range of articles.
for num in range(ARTICLES_BEGIN, ARTICLES_END):

    query = queries[num]

    results = extractor.google(query)

    text = []
    # Write text data for each url
    for result in results:

        req = None
        resp = None
        respData = None
        try:
            req = urllib.request.Request(result, headers=headers)
            resp = urllib.request.urlopen(req)
            respData = resp.read()
        except Exception as e:
            print("Could not open: " + str(query))
            print(e)

        if resp is None or req is None or respData is None:
            continue

        body = None
        soup = None
        try:
            soup = BeautifulSoup(respData, "html.parser")
            for script in soup(["script", "style"]):
                script.extract()
            if soup.body is not None:
                body = soup.body.get_text()
        except Exception as e:
            print("Couldn't extract body")
            print(e)

        if body is None or soup is None:
            continue

        text.append(re.sub(pattern, "", body))

    # Run the text through the
    features = terms.transform(text).toarray()

    neural_pred = big_model.predict(features)
    print("Neural")
    print(big_model.predict(features))
    print(big_model.predict_classes(features))
    entry = {
        "query": query,
        "predictions": neural_pred.tolist()
    }

    try:
        if num >= ARTICLES_END - 1:
            dp.write("{\"add\":%s}" % json.dumps(entry))
        else:
            dp.write("{\"add\":%s}," % json.dumps(entry))
    except Exception as e:
        print("Could not write json")
        print(e)


try:
    dp.write("],")
    dp.write("\"commit\":{}}")
    dp.close()
except Exception as e:
    print("Could not clean up at the end.")
    print(e)
print("Dumped  urls to dump.json")
print("")
