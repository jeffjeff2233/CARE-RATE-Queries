"""
Script Description:
Extracts queries from the anchor text query log.
More details can be found here: http://lemurproject.org/clueweb09/anchortext-querylog/
"""

import gzip
import time
import os

anchor_directory = "D:\\Clue-500M-Anchor-Log-External\\Clue-500M-Anchor-Log-External\\anchor-log-000"

destination = "C:\\Users\\Lanius\\Desktop\\anchor-log-00000.gz"

query_dest = "./queryDump\\relevant_anchor_"

log_strs = []

if not os.path.exists('./queryDump'):
    os.makedirs('./queryDump')

query_strs = []

for num in range(30):
    if num < 10:
        log_strs.append(anchor_directory + "0" + str(num) + ".gz")
    else:
        log_strs.append(anchor_directory + str(num) + ".gz")

    query_strs.append(query_dest + str(num) + ".txt")

for num in range(10, 30):
    print(num)

    t0 = time.time()

    lib = gzip.open(log_strs[num])
    relevant = list()
    for line in lib:
        str_line = str(line)
        firstT = str_line.find("\\t")
        secondT = str_line.find("\\t", firstT+3)
        sub = str_line[firstT + 2:secondT]

        if (sub.find("symptoms") != -1 or
            sub.find("risk factors") != -1 or
            sub.find("disease") != -1 or
            sub.find("cancer") != -1 or
            sub.find("medic") != -1 or
                sub.find("diagnosis") != -1) and len(sub) <= 50:
                relevant.append(sub)

    t1 = time.time()

    noDuplicate = list(set(relevant))

    # Now check for "Blacklisted" terms
    for line in noDuplicate:
        if line.find("fetish") != -1 or line.find("webcam") != -1 \
                or line.find("bdsm") != -1 or line.find("http") != -1:
                noDuplicate.remove(line)

    f = open(query_strs[num], 'w')

    for text in set(noDuplicate):
        f.write(text + '\n')

    f.close()

    print(t1-t0)

    print(len(noDuplicate))

    print(len(relevant))

    lib.close()

