
import urllib as urlmanager
from bs4 import BeautifulSoup
import re
import WebCrawler


class WebScraper(object):
    """
    Given a path to a file containing a list of "seed" phrases, google each of the phrases
    and write the links to a file.
    Based upon the web scraper by Stefania Raimondo.
    """
    def __init__(self, path):
        self.path = path

    def scrape(self, path, scrape_more):
        """

        :param path: File to write URLs to.
        :type path: str
        :param scrape_more: Whether or not the user wishes to scrape the documents found for more links.
        :type scrape_more: bool
        :return: Set of URLS produced from both google searching and document scraping.
        :rtype: str []
        """

        seeds = self.path

        f = open(seeds, 'r')

        urls = f.readlines()

        f.close()

        total_qurls = []

        for base_url in urls:

            print(base_url)

            base_url = base_url.strip()

            base_url = re.sub('\?', '%3F', base_url)

            base_url = re.sub("'", '%27', base_url)

            base_url = re.sub('"', '', base_url)

            base_url = re.sub("â€™", '%27', base_url)

            base_url = re.sub('\s', '+', base_url)

            print(base_url)

            url = 'https://www.google.ca/search?q=' + base_url

            # https://www.google.ca/search?q=random+test&start=10

            headers = {
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6)'
                              ' AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}

            um = urlmanager.request.Request(url, headers=headers)

            resp = urlmanager.request.urlopen(um)
            type(resp)
            respdata = resp.read()

            soup = BeautifulSoup(respdata, "html.parser")

            bss = soup.find_all('div', class_='g')

            qurls = []

            for bs in bss:

                h3s = bs.find_all('h3')

                if len(h3s) == 0:

                    h3s = bs.find_all('div', class_="r")

                    if len(h3s) == 0:
                        print('...none found')

                        continue

                h3as = h3s[0].find_all('a')

                if len(h3as) == 0:

                    h3s = bs.find_all('div', class_="r")

                    if len(h3s) == 0:
                        print('...none found')

                        continue

                    h3as = h3s[0].find_all('a')

                    if len(h3as) == 0:
                        print('...none found')

                        continue

                qurl = h3as[0]['href']

                qurls.append(qurl)

            print(qurls)

            print(len(qurls))

            if len(qurls) == 0:
                raise ValueError

            total_qurls.extend(qurls)

            print('----')

            print()

        print()

        print('total urls:')

        print(len(total_qurls))

        print(len(set(total_qurls)))

        f = open(path, 'w')

        for url in set(total_qurls):
            f.write(url + '\n')

        f.close()

        # If the user wishes to extract links from each of the pages found in the google search(es).
        if scrape_more:

            crawl = WebCrawler.WebCrawler(path)
            crawled_links = crawl.extract_links('crawled_urls.txt')

            total_qurls.extend(crawled_links)

            print('total urls, with crawled URLS:')

            print(len(total_qurls))

            print(len(set(total_qurls)))

            f = open(path, 'w')

            for url in set(total_qurls):
                f.write(url + '\n')

            f.close()

        return set(total_qurls)
